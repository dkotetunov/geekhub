package storage;

import objects.Entity;
import objects.Ignore;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of {@link .storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link .objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "select * from" + clazz.getSimpleName();
        try (Statement stmt = connection.createStatement()) {
            return extractResult(clazz, stmt.executeQuery(sql));

        }
        //implement me according to interface by using extractResult method
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        //implement me
        if (entity.isNew()) return false;
        String sql = "delete from" + entity.getClass().getSimpleName() + "where id" + entity.getId();
        try (Statement stmt = connection.createStatement()) {
            return stmt.executeUpdate(sql) > 0;

        }

    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        String sql = null;
        String table = entity.getClass().getSimpleName();
        if (entity.isNew()) {
            StringBuilder fields = new StringBuilder();
            StringBuilder values = new StringBuilder();
            for (String key : data.keySet()) {
                if (fields.length() > 0) {
                    fields.append(", ");
                }
                fields.append(key);

            }
            sql = "insert into" + table + "()" + fields + ")" + "values" + "(" + values + ")";
        } else {
            //implement me
            //need to define right SQL query to update object
            StringBuilder fieldsSets = new StringBuilder();
//            for (String key : data)

        }
        try (PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            for (Object o : data.values()) {
//                stmt.
            }
            stmt.executeUpdate();
            ResultSet rs = stmt.getGeneratedKeys();
            while (rs.next()) {

                entity.setId(rs.getInt(1));
            }


        }

        //implement me, need to save/update object and update it with new id if it's a creation
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> result = new HashMap<>();
        Field[] fields = entity.getClass().getDeclaredFields();
        {


        }
        return null;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet rs) throws Exception {
        List<T> result = new ArrayList<>();
        while (rs.next()) {
            T entity = clazz.newInstance();
            entity.setId(rs.getInt("id"));
            for (Field field : entity.getClass().getDeclaredField()) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(Ignore.class))
                    continue;

                Object value = rs.getObject(field.getName());
                field.set(entity, value);
            }
        }
        return result;
    }
}
