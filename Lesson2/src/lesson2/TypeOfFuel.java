package lesson2;

public interface TypeOfFuel {
    void refillAndMoney(int volume);
    double consumption(int distance);
}