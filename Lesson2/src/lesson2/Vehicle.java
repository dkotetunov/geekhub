package lesson2;

public abstract class  Vehicle implements Driveable {
    public abstract int accelerate(Boolean force);
    public abstract void brake();
    @Override
    public String turn(String side){
        return "Повернути машину в "+side;
    }
}