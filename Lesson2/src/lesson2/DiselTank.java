package lesson2;

public class DiselTank implements TypeOfFuel {
    final int maxTankVolume = 50;
    int myTank;
    @Override
    public void refillAndMoney(int volume){
        myTank = volume;
        double money = volume * 1.15;
        System.out.println("Ваш бак поповнено Солярою на: "+myTank+"\nЗ вас "+money+"$");
    }
    @Override
    public double consumption(int distance){
        double needGas = distance * 0.07; // З розрахунку 7 літрів на 100 км
        double difference = myTank - needGas;
        if(myTank > needGas){
            System.out.println("Пального достатньо, в запасі: "+difference);
        }
        else{
            System.out.println("Пального НЕ достатньо, дозаправ ще: "+(-1*difference));
        }
        return difference;
    }
}