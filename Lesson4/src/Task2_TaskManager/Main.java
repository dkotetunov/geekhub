package Task2_TaskManager;

/**
 * Created with IntelliJ IDEA.
 * User: Виктор
 * Date: 13.11.13
 * Time: 1:46
 * To change this template use File | Settings | File Templates.
 */


import java.util.*;

public class Main {
    private static GetTaskManager taskManager = new GetTaskManager();

    public static void main(String[] args) {

        Task task1 = new Task("work");
        task1.setDescription("Work with computer");
        taskManager.addTask(new GregorianCalendar(2013, 11, 12), task1);

        Task task2 = new Task("university");
        task2.setDescription("Do my homework");
        taskManager.addTask(new GregorianCalendar(2013, 11, 11), task2);

        Task task3 = new Task("gym");
        task3.setDescription("Bench press");
        taskManager.addTask(new GregorianCalendar(2013, 11, 13), task3);

        Task task4 = new Task("sport");
        task4.setDescription("Playing basketball");
        taskManager.addTask(new GregorianCalendar(2013, 11, 10), task4);

        Task task5 = new Task("rest");
        task5.setDescription("Meeting with friends");
        taskManager.addTask(new GregorianCalendar(2013, 11, 15), task5);

        System.out.println("Method getCategories():\n" + taskManager.getCategories());
        System.out.println("Method getTasksByCategories():\n" + taskManager.getTasksByCategories());
        System.out.println("Method getTaskByCategory('gym'):\n" + taskManager.getTasksByCategory("gym"));
        System.out.println("Method getTaskForToday(): // 2013, 11, 8\n" + taskManager.getTasksForToday(new GregorianCalendar(2013, 11, 11)));
        System.out.println("Method removeTask(2013,11,15): // rest - Meeting with friends");
        taskManager.removeTask(new GregorianCalendar(2013, 11, 15));
        System.out.println("Method getCategories():\n" + taskManager.getCategories());
    }

    private static void showDate(Calendar date) {
        System.out.print(date.get(Calendar.DATE) + ".");
        System.out.print(date.get(Calendar.MONTH) + ".");
        System.out.print(date.get(Calendar.YEAR));
    }
}