package Task2_TaskManager;

/**
 * Created with IntelliJ IDEA.
 * User: Виктор
 * Date: 13.11.13
 * Time: 1:45
 * To change this template use File | Settings | File Templates.
 */

import com.sun.javafx.tk.Toolkit;

import java.util.*;

public interface TaskManager {
    public void addTask(Calendar date, Task task);

    public void removeTask(Calendar date);

    public Collection<String> getCategories();

    public Map<String, List<Task>> getTasksByCategories();

    public List<Task> getTasksByCategory(String category);

    public List<Task> getTasksForToday(GregorianCalendar calendar);
}