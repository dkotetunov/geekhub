package Task2_TaskManager;

/**
 * Created with IntelliJ IDEA.
 * User: Виктор
 * Date: 13.11.13
 * Time: 1:48
 * To change this template use File | Settings | File Templates.
 */

import java.sql.*;
import java.util.*;
import java.util.Date;

public class GetTaskManager implements TaskManager {
    private Map<Calendar, Task> tasksMap = new HashMap<Calendar, Task>();

    @Override
    public void addTask(Calendar date, Task task) {
        tasksMap.put(date, task);
    }

    @Override
    public void removeTask(Calendar date) {
        tasksMap.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Set<String> categories = new HashSet<String>();
        for (Task tasks : tasksMap.values()) {
            categories.add(tasks.getCategory());
        }
        return categories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> tasks = new ArrayList<Task>();
        for (Task task : tasksMap.values()) {
            if (category == task.getCategory()) {
                tasks.add(task);
            }
        }
        return tasks;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> tasksByCategories = new HashMap<String, List<Task>>();
        for (Task task : tasksMap.values()) {
            tasksByCategories.put(task.getCategory(), getTasksByCategory(task.getCategory()));
        }
        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksForToday(GregorianCalendar calendar) {
        List<Task> tasks = new ArrayList<Task>();
        for (Calendar date : tasksMap.keySet()) {
            if (calendar.get(Calendar.DATE) == date.get(Calendar.DATE)) {
                tasks.add(tasksMap.get(date));
            }
        }
        return tasks;
    }
}