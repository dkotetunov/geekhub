package Task2_TaskManager;

/**
 * Created with IntelliJ IDEA.
 * User: Виктор
 * Date: 13.11.13
 * Time: 1:46
 * To change this template use File | Settings | File Templates.
 */
public class Task {
    private String category;
    private String description;

    Task(String category) {
        this.category = category;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return category + "- " + description;
    }
}
