package lesson2;

public abstract class CarForWater extends Vehicle {
    protected abstract void boatRun(int vol, int distance);
    public abstract int accelerate(Boolean force);
}
