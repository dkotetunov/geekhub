package lesson2;

public class PetrolCar extends CarForRoad {
    public PetrolCar(int volume, int distance) {
        System.out.println("Ця машинка на бензині їздить))");
        super.carRun(volume,distance);
    }

    @Override
    public int accelerate(Boolean force){
        if(speed>=180){
            System.out.print("Не можливо розігнати більше\n ...ця штука на бензині більше 180 не їде!!!");
        }
        if(force=true && speed<150){
            speed+=40;
        }
        return speed;
    }

}