package com.geekhub.homeworks.lesson2;

public interface Driveable {
    int accelerate(Boolean force);
    void brake();
    String turn(String side);

}