package Task1;

/**
 * Created with IntelliJ IDEA.
 * User: Виктор
 * Date: 29.10.13
 * Time: 14:28
 * To change this template use File | Settings | File Templates.
 */
public class Students implements Comparable{

    public void setStudent(String student) {
        this.student = student;
    }

    private String student;

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public int getIntelligence() {

        return intelligence;
    }

    public String getStudent() {
        return student;
    }

    private int intelligence;

    public Students(int intelligence, String student){

        this.intelligence = intelligence;
        this.student = student;
    }


    @Override
    public int compareTo(Object o) {
        Students anotherPerson = (Students) o;
        if (anotherPerson.intelligence != this.intelligence) {
            if (this.intelligence < anotherPerson.intelligence) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    }

    public String toString(){
        return "student " + student + " have iq " +  intelligence;
    }

}
