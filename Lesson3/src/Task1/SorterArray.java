package Task1;


public class SorterArray {

    public static Comparable[] sort(Comparable[] elements){

        Comparable[] temporaryComp = new Comparable[elements.length];
        temporaryComp = elements.clone();

        for (int i=0; i<elements.length; i++){
            for (int j=0; j<elements.length-1; j++){
                if(temporaryComp[j].compareTo(temporaryComp[j + 1]) == 1){
                    Comparable t =temporaryComp[j];
                    temporaryComp[j]=temporaryComp[j+1];
                    temporaryComp[j+1]=t;
                }
            }
        }
        return temporaryComp;
    }
}
