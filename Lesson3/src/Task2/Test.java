package Task2;


public class Test {
    public static void main(String[] args) {
       testString();
        testStringBuilder();
        testStringBuffer();
    }

    private static void testString() {
        long startTime = 0, endTime;
        String string = "Dimon";
        System.out.println("\nString:");
        for (int i = 0; i < 100000; i++) {
            String p = "Kotetunov";
            string += p;
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));
        System.out.println(" Text: " + string);
    }
    /*
    String:
 Start time - 1383728608880
     */

    private static void testStringBuffer() {
        long startTime = 0, endTime;
        StringBuffer stringBuffer = new StringBuffer();
        System.out.println("\nStingBuffer:");
        for (int i = 0; i < 10000; i++) {
            String p = "Dimon";
            p += "Kotetunov";
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }

            stringBuffer.append(p);
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));
        System.out.println(" Text: " + stringBuffer);
    }
    /*
    StingBuffer:
    Start time - 1383728459051
    End time -   1383728459054
    Difference - 3
    Text: DimonKotetunovDimonKotetunovDimonKotetunovDimonKotetunov

     */

    private static void testStringBuilder() {
        long startTime = 0, endTime;
        StringBuilder stringBuilder = new StringBuilder();
        System.out.println("\nStingBuilder:");
        for (int i = 0; i < 10000; i++) {
            String p = "Dimon";
            p += "Kotetunov";
            if (i == 0) {
                startTime = System.currentTimeMillis();
                System.out.println(" Start time - " + startTime);
            }
            stringBuilder.append(p);
        }
        endTime = System.currentTimeMillis();
        System.out.println(" End time -   " + endTime);
        System.out.println(" Difference - " + (endTime - startTime));
        System.out.println(" Text: " + stringBuilder);
    }

/*    StingBuilder:
    Start time - 1383728459019
    End time -   1383728459031
    Difference - 12
     Text: DimonKotetunovDimonKotetunovDimonKotetunovDimonKotetunov
  */
}
