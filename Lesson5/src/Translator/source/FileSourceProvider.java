package Translator.source;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File file = new File(pathToSource);
        return file.canRead();
    }

    @Override
    public String load(String pathToSource) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(pathToSource));
        String str = "";
        String tmp;
        while ((tmp = in.readLine()) != null)
            str = str + "\n" + tmp;
        in.close();
        return str;
    }
}